import Vue from 'vue'
import App from './App.vue'
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import utils from './utils'
import router from './router'
// import Cesium from 'cesium/Build/Cesium/Cesium'
// import 'cesium/Build/Cesium/Widgets/widgets.css'
 
 
// import echarts from 'echarts'
// Vue.prototype.$echarts = echarts




//粒子效果
import vueParticleLine from 'vue-particle-line'
import 'vue-particle-line/dist/vue-particle-line.css'

Vue.use(vueParticleLine)


Vue.use(ElementUI);

Vue.prototype.$utils=utils
// Vue.prototype.Cesium=Cesium;
Vue.config.productionTip = false

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  render: h => h(App),
}).$mount('#app')
